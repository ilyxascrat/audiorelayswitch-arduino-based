/**
   sketch for audio commutation/switching
   $ver: 0.1
   $author: ilyxa
   BOM:
   - 1 x opto relay board (looks like solid-state relays is not a good idea for commutation, check this statement later)
   - 1 x Arduino Pro Mini (any if no OLED, only ~1400 bytes of code)
   - 1 x 1kOhm potensiometer
   - 1 x 1kOhm resistor
   - box
   - wires
   - 10 x RCA male wall-mounted connectors
   - 1 x USB "external" board

   State:
   - device is Off - unplugged - no commutation at all
   - device is On - triggered to selected channel
   - rotate - switch to selected channel after 1 second delay

   Serice function (as not a part of functionality - indication in my case) beginning from __ sings, can be safetly ignored, this only decoration`  c1f111111111KL"
*/

//#define __DISPLAY // comment this if no display support
#define __LED
/**
    LED section from adafruit example
*/
#ifdef __DISPLAY
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define switchDelay 1500

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);
void __initDisplay() {
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x32)
  display.ssd1306_command(SSD1306_SETCONTRAST);
  display.ssd1306_command(0); // Where c is a value from 0 to 255 (sets contrast e.g. brightness)
}

void __printDisplay(char comment[], int chan, char dir) {
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setCursor(40, 0);
  display.setTextSize(1);
  display.println(comment);
  if (chan < 255) {
    display.setTextSize(3);
    display.setCursor(50, 10);
    display.print(chan);
  }
  if (dir == "r") {
    display.startscrollright(0x00, 0x0F);
  } else {
    display.startscrollleft(0x00, 0x0F);
  }

  display.display();
  if (comment == "switch to ...") { // quick and dirty, some animation
    delay(switchDelay);
  }
}
#else
#define __initDisplay(...)
#define __printDisplay(...)
#endif

#define ON LOW
#define OFF HIGH
#define rdelay delay(500) // mechanical delay
int chan[] = {6, 7, 8, 9}; //Arduino Pro Mini chineese work fine ;)
enum { chan_num = sizeof(chan) / sizeof(int) - 1}; // 0,1,etc - zero means one channel so on, real constant
int chan_ind = 0; // default relay
int chan_ind_last;
enum { _adc_max = 1024}; // 1024 possible values 0..1023
int enco = A0; // ADC P4 channel
int enco_val;
int enco_val_last;
const int enco_val_fix = 5; // value for fixing rattling
int enco_divider = _adc_max / (chan_num + 1);


#ifdef __LED
int chanLED[] = {2, 3, 4, 5}; // quick and dirty, MUST be same number as chan is
void __initLED() {
  for (int i = 0; i <= chan_num; i++) {
    pinMode(chanLED[i], OUTPUT);
    digitalWrite(chanLED[i], HIGH);
    delay(55);
    digitalWrite(chanLED[i], LOW);
    delay(55);
    digitalWrite(chanLED[i], HIGH);
    delay(55);
    digitalWrite(chanLED[i], LOW);
    delay(55);
  }
}
void __lswitch(int channel, int state) { // HIGH is ON, LOW is OFF
  digitalWrite(channel, state);
}
#else
#define __initLED(...)
#define __lswitch(...)
#endif



void rswitch(int channel, int state) {
  digitalWrite(channel, state);
  if (state = ON) {
    rdelay;
  }
}


void setup() {
  for (int i = 0; i <= chan_num; i++) {
    pinMode(chan[i], OUTPUT);
    rswitch(chan[i], OFF);
  }
  enco_val = analogRead(enco);
  enco_val_last = enco_val;
  chan_ind = enco_val / enco_divider;
  chan_ind_last = chan_ind;
  rswitch(chan[chan_ind], ON);

  __initDisplay();
  __printDisplay("CHANNEL", chan_ind, "l");
  __initLED(); // 1 second flashing for all channels
  __lswitch(chanLED[chan_ind], HIGH);
}

void loop()
{
  enco_val = analogRead(enco);
  chan_ind = enco_val / enco_divider;   // channel calculation
  if (((chan_ind != chan_ind_last)) && ((enco_val > enco_val_last + enco_val_fix) || (enco_val < enco_val_last - enco_val_fix)))
  {
    rswitch(chan[chan_ind_last], OFF);          // switch off prev channel
    __printDisplay("switch to ...", chan_ind, "r");
    __lswitch(chanLED[chan_ind_last], LOW);
    rswitch(chan[chan_ind], ON);                // switch on new channel
    __printDisplay("CHANNEL", chan_ind, "r");
    __lswitch(chanLED[chan_ind], HIGH);
  }
  enco_val_last = enco_val;
  chan_ind_last = chan_ind;
  delay(650);
}
