//simu https://www.tinkercad.com/things/cGVyF5MTKP4-sizzling-duup-jaagub/editel?tenant=circuits

/**
   micronucleus/firmware/configuration/t85_default/Makefile.inc:FUSEOPT = -U lfuse:w:0xe1:m -U hfuse:w:0xdd:m -U efuse:w:0xfe:m
   P5 is disabled / not in use (chinesee clone), fuse is in safe position
  firmware - changed timeout
   micronucleus/firmware/configuration/t85_default/bootloaderconfig.h: *  AUTO_EXIT_NO_USB_MS        The bootloader will exit after this delay if no USB is connected.
   micronucleus/firmware/configuration/t85_default/bootloaderconfig.h: *  AUTO_EXIT_MS               The bootloader will exit after this delay if no USB communication
   micronucleus/firmware/configuration/t85_default/bootloaderconfig.h:#define AUTO_EXIT_NO_USB_MS    500
   micronucleus/firmware/configuration/t85_default/bootloaderconfig.h:#define AUTO_EXIT_MS           6000
  firmware - changed bootloader entering (PB5 reset is in use)
   micronucleus/firmware/configuration/t85_default/bootloaderconfig.h:#define ENTRYMODE ENTRY_EXT_RESET


   connection
            P0          P1        P2          P3          P4      P5
            RB_IN1/IN2  RB_IN3/4  RB_IN5/IN6  RB_IN7/IN8  R       reset
   chan_ind 0            1        2           3
   enco                                                   2
*/

#define ON LOW
#define OFF HIGH
#define rdelay delay(50) // mechanical delay


// relay channel definition
int chan[] = {0, 1, 2, 3}; // P2 used as ADC 1, P3+P4 = USB data +/-!! use with care
enum { chan_num = sizeof(chan) / sizeof(int) - 1}; // 0,1,etc - zero means one channel so on, real constant
int chan_ind = 0; // default relay
int chan_ind_last;

// ADC input definition
enum { _adc_max = 1024}; // 1024 possible values 0..1023
int enco = 2; // ADC P4 channel
int enco_val;
int enco_val_last;
const int enco_val_fix = 50; // value for fixing rattling
int enco_divider = _adc_max / (chan_num + 1);

void rswitch(int channel, int state) {
  digitalWrite(channel, state);
  if (state = ON) {
    rdelay;
  }
}

void setup() {
  for (int i = 0; i <= chan_num; i++) {
    pinMode(chan[i], OUTPUT);
    rswitch(chan[i], OFF);
  }
  
  enco_val = analogRead(enco);
  enco_val_last = enco_val;
  chan_ind = enco_val / enco_divider;
  chan_ind_last = chan_ind;

  rswitch(chan[chan_ind], ON);
}

void loop()
{
  enco_val = analogRead(enco);
  chan_ind = enco_val / enco_divider;   // channel calculation
  if ((chan_ind != chan_ind_last))
  {
    rswitch(chan_ind_last, OFF);          // switch off prev channel
    rswitch(chan_ind, ON);                // switch on new channel
    enco_val_last = enco_val;
  }
  chan_ind_last = chan_ind;
  delay(200);
}
